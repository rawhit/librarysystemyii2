<?php

namespace app\controllers;

use Yii;
use app\models\IssueHistory;
use app\models\IssueHistorySearch;
use yii\web\Controller;
use app\models\Books;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IssueHistoryController implements the CRUD actions for IssueHistory model.
 */
class IssueHistoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IssueHistory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IssueHistorySearch();

        $userid = Yii::$app->user->identity->id;
        $params = Yii::$app->request->queryParams;

        // Add userid as a filter in case the user is not admin
        if(Yii::$app->user->identity->role != 'admin')
        {
            $filter = ['userid' => $userid];
            if(isset($params['IssueHistorySearch']))
            {
                $filter = array_merge($filter, $params['IssueHistorySearch']);
            }
            $params = ['IssueHistorySearch' => $filter];
        }
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreatePending()
    {
        $book_id = Yii::$app->request->get()['id'];

        $model = new IssueHistory();
        $model->book_id = $book_id;
        $model->userid = Yii::$app->user->identity->id;
        $model->issue_date = date('Y-m-d H:i:s');
        $model->status = 'pending';
        $model->save();

        $book = Books::findOne($book_id);
        $book->issued_status = 'pending';
        $book->save();

        return yii::$app->runaction('books/index');
    }
    public function actionApprove()
    {
        $issue_id = Yii::$app->request->get()['id'];

        $issueHistory = IssueHistory::findOne($issue_id);
        $issueHistory->status = 'approved';
        $issueHistory->book->issued_status = 'issued';
        $issueHistory->save();
        $issueHistory->book->save();

        return yii::$app->runaction('issue-history/index');
    }
    
    /**
     * Displays a single IssueHistory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IssueHistory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IssueHistory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->issue_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IssueHistory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->issue_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IssueHistory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IssueHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IssueHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IssueHistory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
