<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IssueHistory;

/**
 * IssueHistorySearch represents the model behind the search form of `app\models\IssueHistory`.
 */
class IssueHistorySearch extends IssueHistory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['issue_id', 'userid', 'book_id'], 'integer'],
            [['issue_date', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IssueHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'issue_id' => $this->issue_id,
            'userid' => $this->userid,
            'book_id' => $this->book_id,
            'issue_date' => $this->issue_date,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
