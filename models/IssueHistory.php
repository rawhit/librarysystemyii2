<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "issue_history".
 *
 * @property int $issue_id
 * @property int $userid
 * @property int $book_id
 * @property string $issue_date
 * @property string $status
 *
 * @property Users $user
 * @property Books $book
 */
class IssueHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'issue_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userid', 'book_id', 'issue_date', 'status'], 'required'],
            [['userid', 'book_id'], 'integer'],
            [['issue_date'], 'safe'],
            [['status'], 'string'],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['userid' => 'userid']],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::className(), 'targetAttribute' => ['book_id' => 'book_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'issue_id' => 'Issue ID',
            'userid' => 'Userid',
            'book_id' => 'Book ID',
            'issue_date' => 'Issue Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['userid' => 'userid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Books::className(), ['book_id' => 'book_id']);
    }
}
