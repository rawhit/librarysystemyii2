<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        if (Yii::$app->user->identity->role == 'admin'){
            echo Html::a('Create Books', ['create'], ['class' => 'btn btn-success']);
        } 
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'book_id',
            'book_title',
            'author_name',
            'year',
            'issued_status',

            ['class' => 'yii\grid\ActionColumn',
             'visible' => Yii::$app->user->identity->role == 'admin',
            ],
            [
                'format' => 'raw',
                'visible' => Yii::$app->user->identity->role == 'employee',
                'value' => function($model, $key, $index, $column) {
                    if($model->issued_status == 'available')
                    {
                        return Html::a(
                            '<i class="fa fa-euro">Request</i>',
                            Url::to(['issue-history/create-pending', 'id' => $model->book_id]),
                            [
                                'id'=>'grid-custom-button',
                                'data-pjax'=>true,
                                'action'=>Url::to(['issue-history/create-pending', 'id' => $model->book_id]),
                                'class'=>'button btn btn-default',
                            ]
                        );
                    } else {
                        return '';
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
