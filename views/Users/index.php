<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        if (Yii::$app->user->identity->role == 'admin'){
            echo Html::a('Create Users', ['create'], ['class' => 'btn btn-success']);
        } 
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'userid',
            'username',
            'usertype',
            'name',
            'email:email',
            //'creation_date',

            ['class' => 'yii\grid\ActionColumn',
            'visible' => Yii::$app->user->identity->role == 'admin',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
