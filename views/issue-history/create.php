<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IssueHistory */

$this->title = 'Create Issue History';
$this->params['breadcrumbs'][] = ['label' => 'Issue Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issue-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
