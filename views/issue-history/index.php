<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IssueHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Issue Histories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issue-history-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user.username',
            'book.book_id',
            'book.book_title',
            'issue_date',
            'status',

            [
                'class' => 'yii\grid\ActionColumn',
                'visible' => Yii::$app->user->identity->role == 'admin',
            ],
            [
                'format' => 'raw',
                'visible' => Yii::$app->user->identity->role == 'admin',
                'value' => function($model, $key, $index, $column) {
                    if($model->status == 'pending')
                    {
                        return Html::a(
                            '<i class="fa fa-euro">Approve</i>',
                            Url::to(['issue-history/approve', 'id' => $model->issue_id]),
                            [
                                'id'=>'grid-custom-button',
                                'data-pjax'=>true,
                                'action'=>Url::to(['issue-history/approve', 'id' => $model->issue_id]),
                                'class'=>'button btn btn-default',
                            ]
                        );
                    } else {
                        return '';
                    }
                }
            ]
        ],
    ]); ?>
</div>
