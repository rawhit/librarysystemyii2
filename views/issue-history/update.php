<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IssueHistory */

$this->title = 'Update Issue History: ' . $model->issue_id;
$this->params['breadcrumbs'][] = ['label' => 'Issue Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->issue_id, 'url' => ['view', 'id' => $model->issue_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="issue-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
